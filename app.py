#-*-coding:utf-8-*-

import os
import errno
import json
from flask import Flask, render_template, request, redirect, url_for, abort
from peewee import CharField, BooleanField, ForeignKeyField
from flask_peewee.admin import Admin, ModelAdmin
from flask_peewee.auth import Auth, BaseUser
from flask_peewee.db import Database
from flask.ext.mustache import FlaskMustache
from utils import DictObject


app = Flask(__name__)
app.config.from_pyfile('config.py')

db = Database(app)


# ----------Models---------------

class Language(db.Model):
    name = CharField()
    abbr = CharField() # 缩写

    def __unicode__(self):
        return '%s (%s)' % (self.name, self.abbr)


class Test(db.Model):
    name = CharField()
    readable_name = CharField(default='')

    def __unicode__(self):
        return '%s: %s', (self.name, self.readable_name)

    def get_language_versions(self):
        return Language.select().join(TestLanguage).join(Test).where(Test.name == self.name)

    def languages(self):
        langs = self.get_language_versions()
        return ', '.join([lang.abbr for lang in langs])

    def add_language_version(self, abbr):
        lang = Language.get(Language.abbr == abbr)
        if lang:
            tl = TestLanguage(test=self, language=lang)
            tl.save()


class TestAdmin(ModelAdmin):
    columns = ('name', 'readable_name', 'languages')


class TestLanguage(db.Model):
    test = ForeignKeyField(Test)
    language = ForeignKeyField(Language)

    def __unicode__(self):
        return '%s - %s' % (self.test.name, self.language.abbr)


class User(db.Model, BaseUser):
    username = CharField()
    password = CharField()
    active = BooleanField(default=True)
    admin = BooleanField(default=False)


class UserAdmin(ModelAdmin):
    columns = ('username', 'admin')

    def save_model(self, instance, form, adding=False):
        orig_password = instance.password
        user = super(UserAdmin, self).save_model(instance, form, adding)
        if orig_password != form.password.data:
            user.set_password(form.password.data) 
            user.save() 
            return user


class CustomAuth(Auth):
    def get_user_model(self):
        return User

    def get_model_admin(self, model_admin=None):
        return UserAdmin


# ----------Models End---------------

auth = CustomAuth(app, db)

admin = Admin(app, auth, branding='Admin Panel')
auth.register_admin(admin)
admin.register(Language)
admin.register(Test, TestAdmin)
admin.register(TestLanguage)
admin.setup()

FlaskMustache(app)
app.jinja_env.add_extension('pyjade.ext.jinja.PyJadeExtension')


with open('static/share.json') as fp:
    content = fp.read()
    share_services = json.loads(content)


@app.route('/')
@auth.login_required
def index():
    tests = Test.select()
    data = {
        test.name: [
            {'name': lang.name, 'abbr': lang.abbr} for lang in test.get_language_versions()
        ] for test in tests
    }
    return render_template('index.html', tests=tests, data=json.dumps(data))


def _make_dirs(dir_path):
    try:
        os.makedirs(dir_path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(dir_path):
            pass
        else: 
            raise

def save_questions_to_json_file(name, lang_code, questions_json, is_tmp=False):
    dir_path = 'static/%s/%s/' % ('output_tmp' if is_tmp else 'output', name)
    _make_dirs(dir_path)
    file_name = '%s.%s.json' % (name, lang_code)
    json_path = os.path.join(dir_path, file_name)
    with open(json_path, 'w') as fp:
        fp.write(questions_json.encode('utf-8'))

    return json_path

def save_html_to_file(name, lang_code, html_str):
    dir_path = 'static/output/%s/' % name
    _make_dirs(dir_path)
    file_name = '%s.%s.html' % (name, lang_code)
    html_path = os.path.join(dir_path, file_name)
    with open(html_path, 'w') as fp:
        fp.write(html_str.encode('utf-8'))
    return html_path

def render(request, tmp=False):
    name = request.form.get('meta-name')
    lang_code = request.form.get('meta-lang-code', 'en')
    url = app.config['SITE_URL'] + name + '/' + lang_code
    questions = request.form.get('questions-data', "[]")
    source_path = save_questions_to_json_file(name, lang_code, questions, is_tmp=tmp)

    share_service_ids = request.form.get('share-service-ids', '').split(',')
    selected_shares = []
    for s in share_services:
        if s['id'] in share_service_ids:
            s['code'] = s['code'].replace('{{url}}', url)
            selected_shares.append(s)

    content = DictObject({
        'title': request.form.get('meta-title', 'Title'),
        'description': request.form.get('meta-desc', ''),
        'tip_title': request.form.get('tip-title', 'Tips'),
        'tip_content': request.form.get('tip-content', '').replace('\n', '<br>'),
        'start_button_text': request.form.get('start-button-text', 'Start'),
        'result_tab_title': request.form.get('result-title', 'Result'),
        'more_tab_title': request.form.get('more-title', 'More'),
        'share_tab_title': request.form.get('share-title', 'Share'),
        'result_hint_title': request.form.get('result-desc', 'Your Result:'),
        'evaluating_text': request.form.get('evaluating-text', 'Evaluating...'),
        'result_more_html': request.form.get('result-more-html', ''),
        'shared_stat_part1': request.form.get('shared_stat_part1', ''),
        'shared_stat_part2': request.form.get('shared_stat_part2', ''),
        'share_hint': request.form.get('more_share_hint', ''),
        'share_list': selected_shares,
    })

    meta = DictObject({
        'lang_code': lang_code,
        'url': url,
        'ga_id': '',
        'source_url': app.config['SITE_URL'] + source_path,
        'translators_name': ''
    })
    
    result_value_calc_code = request.form.get('result-value-calc-code', '')
    result_desc_calc_code = request.form.get('result-desc-calc-code', '')

    html = render_template('site/template.jade', **locals())
    return name, lang_code, html

@app.route('/create', methods=['GET', 'POST'])
@auth.login_required
def create():
    languages = Language.select()
    if request.method == 'GET':
        return render_template('create.html', languages=languages)

    name, lang_code, html = render(request)
    save_html_to_file(name, lang_code, html)
    return redirect(url_for('show_test', name=name, lang_code=lang_code))

@app.route('/preview', methods=['POST'])
@auth.login_required
def preview():
    name, lang_code, html = render(request, tmp=True)
    return html


@app.route('/edit', methods=['POST'])
@auth.login_required
def edit():
    return str(request.form)


@app.route('/translate', methods=['POST'])
@auth.login_required
def translate():
    return str(request.form)


@app.route('/<name>/')
@app.route('/<name>/<lang_code>')
def show_test(name, lang_code='en'):
    path = 'static/output/%s/%s.%s.html' % (name, name, lang_code)
    if not os.path.exists(path):
        abort(404)

    with open(path) as fp:
        html = fp.read()
    return html


if __name__ == '__main__':
    auth.User.create_table(fail_silently=True)
    Language.create_table(fail_silently=True)
    Test.create_table(fail_silently=True)
    TestLanguage.create_table(fail_silently=True)
    app.run(host='0.0.0.0')
